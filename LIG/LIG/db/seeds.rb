# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

 User.create({name: 'Johnson', surname: 'Peter', email:'p.j@my_email.com', password:'Peter'})
 User.create({name: 'Simon', surname: 'Paul', email:'p.s@my_email.com', password:'Paul'})