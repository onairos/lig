
class Certification
	include Mongoid::Document

validates_presence_of :description


field :date, type: DateTime # Time or DateTime ?
field :location, type: String 
field :title, type: String
field :description, type: String

embedded_in :resume

end