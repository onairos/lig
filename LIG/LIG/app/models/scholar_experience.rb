
class ScholarExperience
	include Mongoid::Document

	validates_presence_of :scholar_experience_id, :beginDate, :endDate, :school, :location, :description


	field :scholar_experience_id, type: String
	field :beginDate, type: DateTime # Time or DateTime ?
	field :endDate, type: DateTime # Time or DateTime ?
	field :school, type: String # !English meaning
	field :location, type: String
	field :description, type: String
	field :diploma, type: String

	embedded_in :resume

end