


# exemple at http://www.sampleresume.net/

class Resume 
	include Mongoid::Document

	validates_presence_of :name, :surname, :email, :adresse, :phone_number, :date_of_birth
	 # :professional_experiences, :scholar_experiences, :diplomas              --necessary for a complete resume

	field :name, type: String
	field :surname, type: String
	field :email, type: String


	field :adresse, type: String
	field :phone_number, type: String # integer ?
	field :date_of_birth,  type: DateTime


	field :title, type: String
	field :objective, type: String


	
	embeds_many :skills
	embeds_many :professional_experiences
	embeds_many :scholar_experiences
	embeds_many :certifications
	embeds_many :hobbies

	accepts_nested_attributes_for :skills

	belongs_to :user

end