
class Hobby
	include Mongoid::Document

	validates_presence_of :hobby_id, :description

	field :hobby_id, type: String
	field :description, type: String
	embedded_in :resume

end