
class ProfessionalExperience
	include Mongoid::Document

	validates_presence_of :professionnalExperience_id, :beginDate, :endDate, :location, :company, :description


	field :professionnalExperience_id, type: String
	field :beginDate, type: DateTime # Time or DateTime ?
	field :endDate, type: DateTime # Time or DateTime ?
	field :company, type: String
	field :location, type: String

	field :title, type: String
	field :description, type: String

	embedded_in :resume
	embeds_many :responsabilites



end