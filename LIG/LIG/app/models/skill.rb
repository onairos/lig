
class Skill
	include Mongoid::Document

	validates_presence_of  :skill_order, :description, :score
	validates_numericality_of :score
	validates_inclusion_of :score, in: 0..10
	


	field :skill_order, type: Integer
	field :description, type: String
	field :score, type: Integer

	embedded_in :resume

end