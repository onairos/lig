

class Responsability
	include Mongoid::Document

	validates_presence_of :description


	field :description, type: String
	embedded_in :professional_experience


end