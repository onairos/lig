// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery-ui
//= require jquery_ujs
//= require turbolinks
//= require_tree .
//= require js/vendor/jquery-1.10.1.min.js
//= require js/vendor/jquery.gmap3.min.js
//= require js/vendor/modernizr-2.6.2.min.js
//= require js/LIG.js
//= require js/bootstrap.js
//= require js/jquery.easing-1.3.js
//= require js/main.js
//= require js/plugins.js