class UserMailer < ActionMailer::Base
	default from: "LIG@lig.com"

	def welcome(user)
		@user=user
		mail_to: @user.email, subject: "Welcome has a new member"
	end

end