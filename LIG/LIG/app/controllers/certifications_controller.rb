
class CertificationsController < ApplicationController

	before_action :get_certification, only:[:show, :edit, :update, :destroy]

	def get_certification
		@certification= Certification.find(params[:id])
	end

	def index
		@certifications = Certification.all
	end

	def show
		respond_to do |format|
			format.html #show.html.erb
			format.json { render json :@certification}
			format.xml { render xml :@certification }
	end

	def new
		@certification = Certification.new
	end

	def edit
	end

	def create
		@certification=Certification.new(certification_params)

		respond_to do |format|
		if @certification.save
			format.html {redirect_to @certification, notice: 'Certification created' }
		else
			format.html { render action: "new" }
		end
	end

	def update
		if @resume.update(certification_params)
			redirect_to @certification, notice: 'Certification Updated'
		else
			render action: 'edit'

	end

	def destroy
		@certification.destroy
	end

private 
	def certification_params
		params.require(:certification).permit([:date,:location,:title,:description])
end