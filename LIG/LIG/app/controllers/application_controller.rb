class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :configure_permitted_parameters , if: :devise_controller?
  before_action :authenticate_user! # A modifier si pb avec user_controller voir comportement de authenticate_user! !

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << :name << :surname 
  end

  #config.filter_parameters += [:password, :password_confirmation]
end
