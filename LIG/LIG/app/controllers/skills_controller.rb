
class SkillsController < ApplicationController

	# before_action :check_auth, only:[:edit, :update, :destroy]
	before_action :get_resume_skill, only:[:show, :edit, :update , :destroy]


	def get_resume_skill
		@resume= Resume.find(params[:_id])  # a modifier par la suite  ?
		@skill=@resume.skills.find(params[:skill_id])
	end

	# def check_auth
	# 	if session[:user_id] != @resume.id
	# 		flash[:notice] ="Sorry you can't edit this resume"
	# 		redirect_to #(path a ajouter !)
	# 	end
	# end


	# def index
	# 	@skills=Resume.find(params[:_id]).skills
	# end

	def show
	
		respond_to do |format|
			format.html #show.html.erb
			format.json { render json :@skill}
			format.xml { render xml :@skill}
		end
	end

	def new
		@skill=@resume.skills.new
	end

	def edit
	end

	def create
		@skill=@resume.skills.new(skill_params)
		respond_to do |format|
			if @skill.save
				format.html { redirect_to @resume, notice: 'Skill Created'}

			else
				format.html { render action: "new" }
			end
		end
	end

	def update
	end

	def destroy
		@skill.destroy  # valable pour un sous doc ?
	end

	private
	def skill_params
		params.require(:skill).permit([:skill_order, :description, :score]) 
	end




end