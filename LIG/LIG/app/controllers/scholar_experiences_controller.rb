
class ScholarExperiencesController < ApplicationController
	before_action :check_auth, only:[:edit, :update, :destroy]
	before_action :get_resume_scholar_experience, only:[:show, :edit, :update , :destroy]


	def get_resume_scholar_experience
		@resume= Resume.find(params[:_id])  # a modifier par la suite  ?
		@scholarExperience=@resume.skills.find(params[:scholarExperience_id])
	end

	def check_auth
		if session[:user_id] != @resume.id
			flash[:notice] = "Sorry you can't edit this resume"
			redirect_to #(path a ajouter !)
		end
	end

	def index
		@ScholarExperiences=Resume.find(params[:_id]).scholar_experiences
	end

	def show

		respond_to do |format|
			format.html #show.html.erb
			format.json { render json :@scholarExperience}
			format.xml { render xml :@scholarExperience}
		end
	end

	def new
	end

	def edit
	end

	def create
	end

	def update
	end

	def destroy
		@scholarExperience.destroy
	end

end