

class ResumesController < ApplicationController


	before_action :get_resume, only:[:show, :edit, :update, :destroy]
	#before_action :check_auth, only:[:edit, :update, :destroy]

	
	def get_resume
		@resume= current_user.resumes.find(params[:id])
		#@resume = Resume.find(params[:id])
	end

	def check_auth
		if session[:user_id] != @resume.id
			flash[:notice] ="Sorry you can't edit this resume"
			redirect_to #(path a ajouter !)
		end

	end


	def index
		#@resumes = Resume.where(user_id: current_user[:id])
		@resumes=current_user.resumes 
	end

	def show
		respond_to do |format|
			format.html #show.html.erb
			format.json { render json :@resume}
			format.xml { render xml :@resume}
		end

	end

	def new
		@resume=current_user.resumes.new
	end

	def edit
	end

	def create
		@resume = current_user.resumes.new(resume_params)
		respond_to do |format|
			if @resume.save
				format.html { redirect_to @resume, notice: 'Resume created'}

			else
				format.html { render action: "new" }
			end
		end
	end

	def update
		if @resume.update(resume_params) 
			redirect_to @resume, notice: 'Updated'
		else
			render action: 'edit'
		end
	end

	def destroy
		@resume.destroy
		
	end

private
	def resume_params
		params.require(:resume).permit([:user, :name, :surname, :email, :adresse, :phone_number, :date_of_birth]) # a modifier
	end



end