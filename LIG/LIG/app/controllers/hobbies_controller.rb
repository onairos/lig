
class HobbiesController < ApplicationController


	before_action :check_auth, only:[:edit, :update, :destroy]
	before_action :get_resume_skill, only:[:show, :edit, :update , :destroy]


	def get_resume_hobby
		@resume= Resume.find(params[:_id])  # a modifier par la suite  ?
		@hobby=@resume.skills.find(params[:hobby_id])
	end

	def check_auth
		if session[:user_id] != @resume.id
			flash[:notice] ="Sorry you can't edit this resume"
			redirect_to #(path a ajouter !)
		end
	end



	def index
		@hobbies=Resume.find(params[:_id]).hobbies
	end

	def show
		respond_to do |format|
			format.html #show.html.erb
			format.json { render json :@hobby}
			format.xml { render xml :@hobby}
		end
	end

	def new
	end

	def edit
	end

	# def create
	# 	Skill.where(params.require([:skill_id, :description, :score]).create
	# end

	def update
	end

	def destroy
		@hobby.destroy
	end

end