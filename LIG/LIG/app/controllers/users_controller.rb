
class UsersController < ApplicationController



	before_action :get_user , only:[:show, :edit, :update, :destroy]
	
	


	def get_user
		 if current_user.admin? 
		 	@user= User.find(params[:id]) 
		 else 
		 	@user= current_user  	
		 end
	end


	def index
		if current_user.admin
			@users = User.all
		else
			redirect_to '/', :alert => "You didn't have the right to access that page, you have been redirected"
		end

	end

	def show
		respond_to do |format|
			format.html #show.html.erb
			format.json { render json :@user}
			format.xml { render xml :@user}
			format.js 
		end
	end

	def edit
	end

	def update
	
		if @user.update(user_params)
			redirect_to @user, notice:'Updated'
		else
			render action: 'edit'
		end
		
	end

	def destroy
		@user.destroy
		respond_to do |format|
			format.js
		end
	end



	private
	def user_params
		params.require(:user).permit([:name,:surname,:email,:password])
	end

	
end